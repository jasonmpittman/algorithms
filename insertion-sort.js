/**
  * Insertion Sort Algorithm
  * https://bitbucket.org/jasonmpittman/algorithms
  * http://www.jasonmpittman.com
  *
  * Copyright (c) 2016 Jason M. Pittman
  * Licensed under the MIT license
*/

module.exports = {
	insertionSort : function(input) {
		//
	},

	split : function(list) {
		//split input into array
	},

	swap : function(list[], a, b) {
		//swap b for a in the list
	}
};
